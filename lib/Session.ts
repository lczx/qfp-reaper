import os from 'os';
import path from 'path';
import { NetworkInterfaceInfoIPv4, networkInterfaces } from 'os';
import { mkdir, rm, writeFile } from 'fs/promises';
import dns from 'dns';

import { v4 as uuidv4 } from 'uuid';
import objHash from 'object-hash';
import defaultGateway from 'default-gateway';

import { AgentConfig, config, UrlMeta } from './config';
import { SESSIONS_DIR } from './dirs';
import { createAgent, UserAgent } from './agents';
import PacketSniffer from './PacketSniffer';
import ProfileManager from './ProfileManager';
import CaptureChecker from './CaptureChecker';
import SessionLogger from './SessionLogger';
import {dirExists} from './util';


export const SESSION_LOG_FILENAME = 'session.log';
export const CAPTURE_FILENAME = 'packets.pcapng';
const SESSION_CONFIG_DUNP_FILENAME = 'config.json';
const METADATA_FILENAME = 'metadata.json';
const ALERTS_FILENAME = 'alerts.json';
const KEYLOG_FILENAME = 'sslkeylog';

function getHostAddress(hostname: string): Promise<string> {
  return new Promise((resolve, reject) => {
    dns.lookup(hostname, (err, addr, _) => err == null ? resolve(addr) : reject(err));
  });
}

function getHostAddresses(hostname: string): Promise<string[]> {
  return new Promise((resolve, reject) => {
    dns.lookup(hostname, { all: true },
      (err, addrs) => err == null ? resolve(addrs.filter(a => a.family === 4).map(a => a.address)) : reject(err)
    );
  });
}

function getLocalInterface() {
  const { interface: ifc } = defaultGateway.v4.sync();
  for (let addr of networkInterfaces()[ifc]!)
    if (addr.internal === false && addr.family === 'IPv4') return { name: ifc, ...addr }
  return null;
}

type SessionArgs = { agents: AgentConfig[], urls: UrlMeta[], ignoreHosts?: string[] } | { initialState: SessionState };

type SessionState = {
  id: string,
  agents: AgentConfig[],
  urls: UrlMeta[],
  ignoreHosts?: string[],
  iu: number,
  ia: number,
  currentCapture?: string
};

type CaptureResult = { clean: boolean, id: string | null, critical: boolean };

class Session {

  private readonly profileManager = new ProfileManager();
  private readonly captureChecker = new CaptureChecker(config.alerts);
  private readonly interface: { name: string } & NetworkInterfaceInfoIPv4;
  private readonly state: SessionState;
  private readonly agents: UserAgent[];
  private readonly sessionPath;

  public logger?: SessionLogger;
  private ignoreIps?: string[];
  private cancelResolve?: () => void;
  private currentTimeout?: [NodeJS.Timeout, () => void];

  constructor(args: SessionArgs) {
    const ifc = getLocalInterface();
    if (ifc == null)
      throw new Error('Cannot obtain local interface IP');
    this.interface = ifc;

    this.state = 'initialState' in args ? args.initialState : {
      id: uuidv4(),
      agents: args.agents,
      urls: args.urls,
      ignoreHosts: args.ignoreHosts,
      iu: 0,
      ia: 0
    };
    this.agents = this.state.agents.map(a => createAgent(a));

    this.sessionPath = path.join(SESSIONS_DIR, this.state.id);
  }

  async run() {
    await mkdir(this.sessionPath, { recursive: true });

    this.logger = new SessionLogger([
      { name: 'Request', color: 'yellowBright' },
      { name: 'Agent', color: 'cyanBright' }
    ], path.join(this.sessionPath, SESSION_LOG_FILENAME));

    const fresh = this.state.iu === 0 && this.state.ia === 0;

    this.logger.log.info((fresh ? 'Started' : 'Resumed') +
                     ` session ${this.state.id} on "${os.hostname()}" ${this.interface.address} (${this.interface.name})`);

    // Store current configuration in session folder
    if (fresh) await writeFile(path.join(this.sessionPath, SESSION_CONFIG_DUNP_FILENAME), JSON.stringify(config));

    // If the application exited during a capture last time, remove it
    if (this.state.currentCapture != null) {
      await this.deleteCapture(this.state.currentCapture);
      this.state.currentCapture = undefined;
    }

    // Lookup ignored hosts
    const dnsProms = this.state.ignoreHosts?.map(h => getHostAddresses(h));
    if (dnsProms != null) this.ignoreIps = (await Promise.all(dnsProms)).flat();

    // Initialize agents
    for (const agent of this.agents) await agent.configure();

    this.logger.initializeStatus([this.state.urls.length, this.agents.length]);

    while (this.state.iu < this.state.urls.length) {
      const urlm = this.state.urls[this.state.iu];

      while (this.state.ia < this.agents.length) {
        const agent = this.agents[this.state.ia];

        this.logger.updateStatus([
          { progress: this.state.iu, label: urlm.url },
          { progress: this.state.ia, label: agent.toString() }
        ]);

        for (let attempt = 0; attempt < config.attempts; ++attempt) {
          if (attempt > 0)
            this.logger.log.warn(`...retrying (attempt ${attempt + 1}/${config.attempts})`);

          const { clean, id, critical } = await this.captureSite(agent, urlm);

          if (critical && attempt === config.attempts - 1)
            throw new Error('Critical capture error on last attempt, aborting');

          if (!clean && attempt < config.attempts - 1) {
            if (id != null) await this.deleteCapture(id);
          } else {
            await this.profileManager.commit();
          }

          if (this.cancelResolve) {
            this.cancelResolve();
            return;
          }

          if (clean) break;
          await this.waitTime(config.timeBetweenAttempts);

          if (this.cancelResolve) {
            // @ts-ignore
            this.cancelResolve();
            return;
          }
        }

        this.state.ia++;

        await this.waitTime(config.timeBetweenCaptures);

        if (this.cancelResolve) {
          this.cancelResolve();
          return;
        }
      }

      this.state.iu++;
      this.state.ia = 0;

      // Wipe profile dirs for the next host
      await this.profileManager.cleanup();
    }

    this.logger.clearStatus();
  }

  dumpState() {
    return this.state;
  }

  cancel() {
    this.logger?.clearStatus();
    if (this.currentTimeout) {
      clearTimeout(this.currentTimeout[0]);
      this.currentTimeout[1]!();
    }
    return new Promise<void>(res => this.cancelResolve = res);
  }

  private async captureSite(agent: UserAgent, urlm: UrlMeta): Promise<CaptureResult> {
    const [profileDir, freshProfile] = await this.profileManager.getProfileDir(agent);
    const captureTag = `${urlm.url} ${agent.toString()}` + (freshProfile ? '' : ' reuse-profile');

    let hostIp;
    try {
      hostIp = await getHostAddress(new URL(urlm.url).hostname);
    } catch (e) {
      this.logger!.log.error(`[${captureTag}] DNS lookup failed: ${e}`);
      return { clean: false, id: null, critical: true };
    }

    const captureMetadata = {
      timestamp: Date.now(),
      agent: agent.agentConfig,
      local: { hostname: os.hostname(), ip: this.interface.address },
      remote: { url: urlm.url, ip: hostIp },
      metadata: urlm.metadata,
      freshProfile
    };
    const captureId = objHash(captureMetadata);
    this.state.currentCapture = captureId;

    this.logger!.log.debug(`${captureId} ${captureTag}`);

    const capturePath = path.join(this.sessionPath, captureId);
    await mkdir(capturePath);

    await writeFile(path.join(capturePath, METADATA_FILENAME), JSON.stringify(captureMetadata));

    const captureFile = path.join(capturePath, CAPTURE_FILENAME);
    const keylogFile = path.join(capturePath, KEYLOG_FILENAME);

    const sniffer = new PacketSniffer(captureFile);

    try {
      await sniffer.startCapture({
        interface: this.interface.name,
        filterIp: this.interface.address,
        excludeIps: this.ignoreIps
      });

      process.env['SSLKEYLOGFILE'] = path.join(process.cwd(), keylogFile);
      await agent.navigate(profileDir, urlm.url);
      process.env['SSLKEYLOGFILE'] = undefined;

      await this.waitTime(config.captureEndDelay);

      await sniffer.endCapture();

      const matchingAlerts = await this.captureChecker.checkCapture(
        this.logger!.log, captureTag, this.interface.address, hostIp, captureFile, keylogFile);
      const clean = matchingAlerts.findIndex(a => a.retry == null || a.retry === true) === -1;
      const critical = matchingAlerts.findIndex(a => a.critical === true) !== -1;

      if (matchingAlerts.length > 0)
        await writeFile(path.join(capturePath, ALERTS_FILENAME), JSON.stringify(matchingAlerts));

      // To signal that this capture has completed successfully
      this.state.currentCapture = undefined;

      return { clean, id: captureId, critical };

    } catch (e) {
      if (sniffer.isRunning())
        sniffer.endCapture();  // Seems that await does not work well here... no problem, we are closing

      // If session is closing, ignore the error
      if (this.cancelResolve == null)
        this.logger!.log.error(`[${captureTag}] Capture error: ${e}`);

      return { clean: false, id: captureId, critical: true };
    }

  }

  private async deleteCapture(captureId: string) {
    const captureDir = path.join(this.sessionPath, captureId);
    if (dirExists(captureDir)) await rm(captureDir, { recursive: true });
  }

  private async waitTime(timeMs?: number) {
    if (timeMs != null && timeMs > 0) {
      return new Promise<void>(res => {
        this.currentTimeout = [
          setTimeout(() => {
            this.currentTimeout = undefined;
            res();
          }, timeMs),
          res
        ];
      });
    }
  }

}

export default Session;
