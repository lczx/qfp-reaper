import { spawn } from 'child_process';

export function waitProcessOutput(command: string, args: string[]): Promise<string> {
  const proc = spawn(command, args);
  let outStr = '';
  let errStr = '';

  proc.stdout.on('data', chunk => outStr += chunk.toString());
  proc.stderr.on('data', chunk => errStr += chunk.toString());

  return new Promise((res, rej) => {
    proc.on('close', code => code === null || code === 0
      ? res(outStr)
      : rej(`${command} exited with code ${code}\n${errStr}`));
    proc.on('error', rej);
  });
}
