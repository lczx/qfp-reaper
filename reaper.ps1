
$NODE_VERSION = '16.14.2'

If (!(Test-Path '.\local\node\node.exe')) {
    New-Item -Path .\local -ItemType Directory -Force | Out-Null
    Write-Host 'Downloading Node.js...'
    Invoke-WebRequest `
        -Uri "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-win-x64.zip" `
        -OutFile '.\local\node.zip'
    Expand-Archive -Path '.\local\node.zip' -DestinationPath '.\local\'
    Remove-Item -Path '.\local\node.zip'
    Rename-Item -Path ".\local\node-v$NODE_VERSION-win-x64" -NewName 'node'
}

$env:COREPACK_HOME = 'local/corepack'

If (!(Test-Path '.yarn')) {
    Write-Host 'Installing dependencies...'
    local/node/corepack yarn install
}

local/node/corepack yarn start $args
