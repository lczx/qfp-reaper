#!/bin/bash

# Derived from:
#   https://gist.github.com/dpino/6c0dca1742093346461e11aa8f608a99

if [[ $EUID -ne 0 ]]; then
    echo 'This script must be run as root'
    exit 1
fi

NS="ns1"
SUBNET="10.100.1.0/24"
VETH="veth1"
VETH_ADDR="10.100.1.1"
VPEER="vpeer1"
VPEER_ADDR="10.100.1.2"

get_interface() {
    ip -4 route show default | cut -d ' ' -f 5
}

clear_iptables() {
    iptables -t nat -D POSTROUTING -s $SUBNET -o "$IFACE" -j MASQUERADE 2> /dev/null
    iptables -D FORWARD -i "$IFACE" -o $VETH -j ACCEPT 2> /dev/null
    iptables -D FORWARD -o "$IFACE" -i $VETH -j ACCEPT 2> /dev/null
    hash firewall-cmd 2> /dev/null && firewall-cmd -q --remove-forward
}

setup_iptables() {
    iptables -t nat -A POSTROUTING -s $SUBNET -o "$IFACE" -j MASQUERADE
    iptables -A FORWARD -i "$IFACE" -o $VETH -j ACCEPT
    iptables -A FORWARD -o "$IFACE" -i $VETH -j ACCEPT
    hash firewall-cmd 2> /dev/null && firewall-cmd -q --add-forward
}

if [[ "$1" == 'up' ]]; then
    IFACE="${2:-$(get_interface)}"
    echo "Using interface \"$IFACE\"" >&2

    # Create namespace
    ip netns del $NS &> /dev/null
    ip netns add $NS
    sleep .2

    # Create veth link and move peer to NS
    ip link add $VETH type veth peer name $VPEER
    ip link set $VPEER netns $NS

    # Setup veth interface
    ip addr add $VETH_ADDR/24 dev $VETH
    ip link set $VETH up

    # Setup peer interface
    ip -n $NS addr add $VPEER_ADDR/24 dev $VPEER
    ip -n $NS link set $VPEER up
    ip -n $NS link set lo up
    ip -n $NS route add default via $VETH_ADDR

    # Ensure forwarding is enabled
    echo 1 > /proc/sys/net/ipv4/ip_forward

    # Bypass systemd-resolved if enabled
    if systemctl -q is-active systemd-resolved.service; then
        DNS_HOST=$(resolvectl -i "$IFACE" dns | awk -F ': ' '{ print $2 }')
        echo "Using nameserver \"$DNS_HOST\"" >&2

        mkdir -p /etc/netns/$NS
        echo "nameserver $DNS_HOST" > /etc/netns/$NS/resolv.conf
        echo 'hosts: files dns' > /etc/netns/$NS/nsswitch.conf
    fi

    # Apply optional hosts file
    if [ -f "$(dirname "$0")/hosts" ]; then
        install -m 644 "$(dirname "$0")/hosts" /etc/netns/$NS/hosts
    fi

    # Enable masquerading and allow traffic from/to veth
    clear_iptables
    setup_iptables

    exit 0

elif [[ "$1" == 'down' ]]; then
    IFACE="${2:-$(get_interface)}"
    echo "Using interface \"$IFACE\""

    ip netns del $NS &> /dev/null
    rm -rf /etc/netns/$NS
    clear_iptables

    exit 0

elif [[ "$1" == 'exec' ]]; then
    if ! ip netns list | grep -q "^$NS"; then
        echo "Network namespace not found, execute \"$(basename "$0") up [iface]\" first"
        exit 1
    fi

    ip netns exec "$NS" runuser -u "${SUDO_USER:-$(logname)}" -- "${@:2}"

else
    echo 'No command provided, use: up, down, exec'
fi
