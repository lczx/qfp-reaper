import { ChildProcess, spawn } from 'child_process';

import { findInPath } from './util';


const CAPTURE_TOOL = process.platform === 'win32' ? 'dumpcap.exe' : 'dumpcap';

type CaptureArgs = {
  onError?: (code: number) => void,
 interface?: string,
 filterIp?: string,
 excludeIps?: string[]
};

function makePcapFilter(includes: string[] | undefined, excludes: string[] | undefined) {
  const inclFilter = includes == null ? [] : includes.map(ip => `host ${ip}`);
  const exclFilter = excludes == null ? [] : excludes.map(ip => `not host ${ip}`);
  return [...inclFilter, ...exclFilter].join(' and ');
}

class PacketSniffer {

  private process: ChildProcess | null = null;
  private endPromise: Promise<void> | null = null;;

  constructor(public readonly dumpFile: string) { }

  static isOperational(): boolean {
    return findInPath(CAPTURE_TOOL) != null;
  }

  async startCapture(args?: CaptureArgs): Promise<void> {
    if (this.process != null)
      throw new Error('Capture already started');

    const binaryPath = findInPath(CAPTURE_TOOL);
    if (binaryPath == null)
      throw new Error(`Capture binary "${CAPTURE_TOOL}" not found`);

    const processArgs = ['-qw', this.dumpFile];
    if (args?.interface != null)
      processArgs.push('-i', args.interface);

    const filter = makePcapFilter(args?.filterIp == null ? undefined : [args.filterIp], args?.excludeIps);
    if (filter !== '') processArgs.push('-f', filter);

    this.process = spawn(binaryPath, processArgs);

    this.endPromise = new Promise((res, rej) => {
      this.process!.on('close', code => {
        this.process = null;
        this.endPromise = null;
        if (code === null || code === 0) {
          res();
        } else {
          args?.onError?.(code);
          rej(code);
        }
      });
    });

    return new Promise((res, rej) => {
      this.process!.on('spawn', res);
      this.process!.on('error', rej);
    });
  }

  endCapture() {
    if (this.process == null)
      throw new Error('Capture not started');

    this.process.kill('SIGINT');
    return this.endPromise;
  }

  isRunning() {
    return this.process != null;
  }

}

export default PacketSniffer;
