import winston, { Logger } from 'winston';
import Transport, { TransportStreamOptions } from 'winston-transport';
import chalk, { ForegroundColor } from 'chalk';
// @ts-ignore
import LogWithStatusBar from 'log-with-statusbar';
// @ts-ignore
import progressString from 'progress-string';


const BAR_WIDTH = 60;

const LOG_FORMAT = winston.format.printf(({ level, message, timestamp, ...args }) => {
  const extra = Object.keys(args).length === 0 ? '' : ` ${JSON.stringify(args)}`;
  return `${timestamp} ${level}: ${message}${extra}`;
});

function makeProgressTransport(recv: (msg: string) => void, opts?: TransportStreamOptions) {
  return new class extends Transport {
    constructor() {
      super(opts);
    }

    log(info: any, callback: () => void) {
      recv(info[Symbol.for('message')]);
      callback();
    }
  }
}

const makeStatus = (bar: any, color: typeof ForegroundColor, index: number, total: number, name: string) => {
  const colTr = chalk[color];
  const perc = (100 * index / total).toFixed(0);

  const pt1 = chalk.bold('[' + bar(index) + '] ' + colTr(perc + '%'));
  const pt2 = colTr(`${index + 1}/${total}`);
  return `${pt1} (${pt2}) ${name}`;
}

type BarSpec = { name: string, color: typeof ForegroundColor };
type ProgressUpdate = { progress: number, label: string };

class SessionLogger {

  private wLogger: Logger;
  private lwst = LogWithStatusBar({ initialStatusTextArray: ['Please wait...'] });
  private totals?: number[];
  private bars?: Array<(progress: number) => string>;
  private nameLen: number;

  constructor(private readonly barSpecs: BarSpec[], logPath?: string) {
    const transports: winston.transport[] = [
        makeProgressTransport(this.lwst, {
          format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
          level: 'info'
        }),
    ];
    if (logPath != null) {
      transports.push(
        new winston.transports.File({
          filename: logPath,
          format: winston.format.combine(winston.format.timestamp(), LOG_FORMAT)
        })
      );
    }

    this.nameLen = Math.max(...barSpecs.map(s => s.name.length));
    this.wLogger = winston.createLogger({ level: 'debug', transports });
  }

  get log() {
    return this.wLogger;
  }

  initializeStatus(totals: number[]) {
    if (this.barSpecs.length !== totals.length)
      throw new Error('Totals array does not match bar specification size');

    this.totals = totals;
    this.bars = Array.from({ length: this.barSpecs.length }, (_, i) => progressString({
      width: BAR_WIDTH,
      total: totals[i],
      style: (complete: string, incomplete: string) => chalk[this.barSpecs[i].color](complete) + chalk.gray(incomplete)
    }));
  }

  updateStatus(updates: ProgressUpdate[]) {
    const lines = Array.from({ length: this.barSpecs.length }, (_, i) => {
      const { name, color } = this.barSpecs[i];
      const { progress, label } = updates[i];
      const title = chalk.bold[color]((name + ':').padEnd(this.nameLen + 2));
      const bar = makeStatus(this.bars![i], color, progress, this.totals![i], label);
      return title + bar;
    });

    this.lwst.setStatusBarText(lines);
  }

  clearStatus() {
    this.lwst.setStatusBarText([]);
  }

}

export default SessionLogger;

