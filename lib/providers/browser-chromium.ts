import { mkdir } from 'fs/promises';
import path from 'path';
import { createWriteStream, mkdirSync } from 'fs';
import { https } from 'follow-redirects';
import unzip from 'unzipper';

import { BROWSERS_DIR } from '../dirs';
import { dirExists } from '../util';


const CHROME_EXECUTABLES = [
  'chrome-linux/chrome',
  'chrome-linux/chrome_crashpad_handler',
  'chrome-linux/chrome_sandbox'
];

function makeChromiumReleaseUrl(platform: NodeJS.Platform, version: string) {
  const base = 'https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/';
  if (platform === 'linux')
    return base + `Linux_x64%2F${version}%2Fchrome-linux.zip?alt=media`;
  else if (platform === 'win32')
    return base + `Win_x64%2F${version}%2Fchrome-win.zip?alt=media`;
  throw new Error(`Unknown platform ${platform}`);
}

async function downloadChromiumBrowser(platform: NodeJS.Platform, version: string) {
  console.log(`Downloading Chromium v${version}`);
  await mkdir(path.join(BROWSERS_DIR, `chromium-${version}`), { recursive: true });
  const url = makeChromiumReleaseUrl(platform, version);

  return new Promise<void>((resolve, reject) => {
    https.get(url, res => res
      .pipe(unzip.Parse())
      .on('entry', entry => {
        const outPath = path.join(BROWSERS_DIR, entry.path.replace(
          platform === 'win32' ? /^chrome-win\// : /^chrome-linux\//,
          `chromium-${version}/`));
        if (entry.type === 'File') {
          const isExe = platform !== 'win32' && CHROME_EXECUTABLES.includes(entry.path);
          mkdirSync(path.dirname(outPath), { recursive: true });
          entry.pipe(createWriteStream(outPath, isExe ? { mode: 0o755 } : undefined));
        } else {
          entry.autodrain();
        }
      })
      .on('finish', resolve)
    ).on('error', reject);
  });
}

export async function getChromiumBrowser(version: string): Promise<string> {
  const browserDir = path.join(BROWSERS_DIR, `chromium-${version}`);

  if (!dirExists(browserDir))
    await downloadChromiumBrowser(process.platform, version);

  const binaryName = process.platform === 'win32' ? 'chrome.exe' : 'chrome';
  return path.join(browserDir, binaryName);
}
