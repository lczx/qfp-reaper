import path from 'path';
import { readdir, writeFile }  from 'fs/promises';

import { fileExists, findInPath } from '../util';
import { CAPTURE_FILENAME, SESSION_LOG_FILENAME } from '../Session';
import { waitProcessOutput } from '../process';
import { TSHARK_BINARY } from '../CaptureChecker';
import SessionLogger from '../SessionLogger';


const ANALYSIS_FILENAME = 'analysis.json';

type DnsResults = { [host: string]: string[] };
type HostIpPair = [host: string, ips: string[]];

function extractDomainAddresses(logger: SessionLogger, wsPacket: any): HostIpPair | null {
  const { Queries: q, Answers: a } = wsPacket._source.layers.dns;

  const qKeys = Object.keys(q);

  if (qKeys.length > 1)
    throw new Error('DNS packets with multiple queries are not supported');

  const qData = q[qKeys[0]];

  if (qData['dns.qry.type'] !== '1') {
    logger.log.warn(`"${qKeys[0]}" has unknown query type`);
    return null;
  }

  const domain = qData['dns.qry.name'];

  return [
    domain,
    Object.values(a)
      .filter((d: any) => d['dns.resp.type'] === '1')
      .map((d: any) => d['dns.a'])
  ];
}

export async function analyzeDns(sessionDir: string) {
  if (!fileExists(path.join(sessionDir, SESSION_LOG_FILENAME)))
    throw new Error(`"${sessionDir}" is not a session directory`);

  const captures = (await readdir(sessionDir, { withFileTypes: true }))
    .filter(d => d.isDirectory())
    .map(d => d.name);

  const binaryPath = findInPath(TSHARK_BINARY);
  if (binaryPath == null)  // dup from CaptureChecker
    throw new Error(`Capture analysis binary "${TSHARK_BINARY}" not found`);

  const logger = new SessionLogger([
    { name: 'Capture', color: 'greenBright' }
  ]);
  logger.initializeStatus([ captures.length ]);

  const results: { [capture: string]: DnsResults } = {};

  for (let i = 0; i < captures.length; ++i) {
    logger.updateStatus([{progress: i, label: captures[i] }])

    const data = await waitProcessOutput(binaryPath,
      ['-r', path.join(sessionDir, captures[i], CAPTURE_FILENAME), '-Y', 'dns && dns.a', '-T', 'json']);

    const dns: DnsResults = Object.fromEntries(JSON.parse(data)
      .map(extractDomainAddresses.bind(null, logger))
      .filter((d: HostIpPair | null)=> d != null));

    results[captures[i]] = dns;
  }


  await writeFile(path.join(sessionDir, ANALYSIS_FILENAME), JSON.stringify({ dns: results }));

  logger.clearStatus();
}
