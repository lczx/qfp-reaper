import { fetchDriver } from './drivers';
import { getFirefoxBrowser } from './browser-firefox';
import { getChromiumBrowser } from './browser-chromium';
import { BrowserType } from '../config';


export const getWebDriverProvider = (browserType: BrowserType): () => Promise<string> => {
  return fetchDriver.bind(null, browserType, process.platform);
};

export const getBrowserProvider = (browserType: BrowserType): (version: string) => Promise<string> => {
  if (browserType === 'firefox') return getFirefoxBrowser;
  if (browserType === 'chromium') return getChromiumBrowser;
  throw new Error(`Unknown browser type ${browserType}`);
};
