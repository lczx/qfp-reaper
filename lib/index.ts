import path from 'path';
import { writeFileSync } from 'fs';
import { readFile, rm } from 'fs/promises';

import { program } from 'commander';

import { SESSIONS_DIR } from './dirs';
import PacketSniffer from './PacketSniffer';
import Session from './Session';
import { wipeProfileData } from './ProfileManager';
import { config, UrlMeta } from './config';
import { analyzeDns } from './analysis/dns';


const SESSION_DUMP_FILE = path.join(config.workDir, 'session-dump.json');

if (!PacketSniffer.isOperational()) {
  console.error('Packet capture cannot be started, consider installing Wireshark to provide the required tools');
  process.exit(-1);
}

program
  .name('reaper')
  .option('--workdir <dir>', 'set working directory');

program
  .command('capture', { isDefault: true })
  .description('capture browser web traces')
  .argument('[urls_file]', 'optional urls file as a configuration override')
  .argument('[label]', 'capture only the given label')
  .option('-c, --clean', 'wipe sessions and browser profiles on startup')
  .action(async (urlFile, label, opts) => {
    if (urlFile && program.opts().workdir)
      urlFile = path.join(program.opts().workdir, urlFile);
    await captureMain(urlFile, label, opts.clean === true);
  });

const programAnalyze = program.command('analyze');

programAnalyze.command('dns <session_dir>')
  .description('analyze resolved DNS hosts in a session captures')
  .action(async (sessionDir) => {
    if (program.opts().workdir)
      sessionDir = path.join(program.opts().workdir, sessionDir);
    try {
      await analyzeDns(sessionDir)
    } catch (e) {
      console.error('Error:', e);
    }
  });

program.parse(process.argv);


async function readUrls(sourceFile: string): Promise<UrlMeta[]> {
  return (await readFile(sourceFile, { encoding: 'utf8' }))
    .split('\n')
    .filter(h => h.length > 0 && !h.startsWith('#'))
    .map(l => {
      const [url, ...dataParts] = l.split(' ');
      const dataRaw = dataParts.join(' ').trim();
      const metadata = dataRaw === '' ? null : JSON.parse(dataRaw);
      return { url, metadata };
    });
}

function storeSession(state: any) {
  writeFileSync(SESSION_DUMP_FILE, JSON.stringify(state));
}

async function recoverSession() {
  try {
    const data = JSON.parse(await readFile(SESSION_DUMP_FILE, { encoding: 'utf8' }));
    await rm(SESSION_DUMP_FILE);
    return data;
  } catch (_) {
    return null;
  }
}

async function captureMain(urlFile: string | undefined, label: string | undefined, clean: boolean) {
  if (clean === true) {
    // This also removes the dump file if it exists
    const sessionDump = await recoverSession();
    if (sessionDump != null)
      await rm(path.join(SESSIONS_DIR, sessionDump.id), { force: true, recursive: true });
    await wipeProfileData();
  }

  const sessionDump = await recoverSession();
  const session = sessionDump != null
    ? new Session({ initialState: sessionDump })
    : await (async () => {
      if (urlFile != null) config.urls = urlFile;
      let urls = await readUrls(config.urls);
      // @ts-ignore
      if (label != null) urls = urls.filter(u => u.metadata.label === label);
      return new Session({ agents: config.agents, urls, ignoreHosts: config.ignoreHosts });
    })();

  process.once('SIGINT', async () => {
    session.logger!.log.info('Session stopped (user request)');
    await session.cancel();
    storeSession(session.dumpState());
  });

  try {
    await session.run();
  } catch (e) {
    storeSession(session.dumpState());
    console.error('Session stopped (error)', e);
    process.exit(1);
  }

  console.log('\x1b[3mfin\x1b[0m');
}
