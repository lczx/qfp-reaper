import { createWriteStream } from 'fs';
import { mkdir, rename, rm } from 'fs/promises';
import path from 'path';
import { https } from 'follow-redirects';
import unbzip2 from 'unbzip2-stream';
import tarfs from 'tar-fs';
import seven from 'node-7z';

// @ts-ignore
import sevenbin from '7zip-binaries'

import { BROWSERS_DIR } from '../dirs';
import { dirExists } from '../util';


function makeFirefoxReleaseUrl(platform: NodeJS.Platform, version: string) {
  const base = `https://ftp.mozilla.org/pub/firefox/releases/${version}/`;
  if (platform === 'linux')
    return base + `linux-x86_64/en-US/firefox-${version}.tar.bz2`;
  else if (platform === 'win32')
    return base + `win64/en-US/Firefox%20Setup%20${version}.exe`;
  throw new Error(`Unknown platform ${platform}`);
}

async function downloadFirefoxBrowser(platform: NodeJS.Platform, version: string) {
  console.log(`Downloading Firefox v${version}`);
  await mkdir(BROWSERS_DIR, { recursive: true });
  const url = makeFirefoxReleaseUrl(platform, version);

  if (platform === 'win32') {
    const instFile = path.join(BROWSERS_DIR, 'ff_inst.exe');

    await new Promise<void>((resolve, reject) => {
      https.get(url, res => {
        const ws = createWriteStream(instFile);
        res.pipe(ws);
        ws.on('finish', () => resolve());
      });
    });

    await new Promise<void>((resolve, reject) => {
      seven.extractFull(instFile, BROWSERS_DIR, {
        $cherryPick: ['core'],
        $bin: sevenbin.bin()
      })
        .on('error', e => reject(e))
        .on('end', () => resolve());
    });

    await rename(path.join(BROWSERS_DIR, 'core'), path.join(BROWSERS_DIR, `firefox-${version}/`));
    await rm(instFile);

  } else {
    return new Promise<void>((resolve, reject) => {
      https.get(url, res => res
        .pipe(unbzip2())
        .pipe(tarfs.extract(BROWSERS_DIR, {
          map: header => {
            header.name = header.name.replace(/^firefox\//, `firefox-${version}/`);
            return header;
          }
        }))
        .on('finish', resolve)
      ).on('error', reject);
    });
  }
}

export async function getFirefoxBrowser(version: string): Promise<string> {
  const browserDir = path.join(BROWSERS_DIR, `firefox-${version}`);

  if (!dirExists(browserDir))
    await downloadFirefoxBrowser(process.platform, version);

  const binaryName = process.platform === 'win32' ? 'firefox.exe' : 'firefox';
  return path.join(browserDir, binaryName);
}
