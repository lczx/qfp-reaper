import path from 'path';
import { createWriteStream } from 'fs';
import { mkdir } from 'fs/promises';
import { IncomingMessage } from 'http';
import { https } from 'follow-redirects';
import unzip from 'unzipper';
import zlib from 'zlib';
import tarfs from 'tar-fs';

import { BrowserType } from '../config';
import { WEBDRIVER_DIR } from '../dirs';
import { fileExists } from '../util';
import manifest from './drivers-manifest.json';


export async function fetchDriver(browser: BrowserType, platform: NodeJS.Platform) {
  await mkdir(WEBDRIVER_DIR, { recursive: true });

  const bm = manifest[browser];
  if (bm == null)
    throw new Error(`Unknown browser type "${browser}"`);

  if (!(platform in bm.platform))
    throw new Error(`Unsupported platform "${platform}" for browser "${browser}"`);

  // @ts-ignore
  const bmp = bm.platform[platform];

  const binaryPath = path.join(WEBDRIVER_DIR, bmp.filename);
  if (fileExists(binaryPath))
    return binaryPath;

  console.log(`Downloading "${browser}" driver...`);

  return new Promise<void>((resolve, reject) => {
    https.get(bm.base + bmp.ext, makeExtractor(bmp.ext, bmp.filename, resolve))
      .on('error', reject);
  }).then(() => binaryPath);
}

function makeExtractor(url: string, filename: string, onFinish: () => void): (r: IncomingMessage) => void {
  if (url.endsWith('.tar.gz')) {
    return res => res
      .pipe(zlib.createGunzip())
      .pipe(tarfs.extract(WEBDRIVER_DIR, {
        filter: n => n === filename,
        fmode: 0o755
      }))
      .on('finish', onFinish)

  } else if (url.endsWith('.zip')) {
    return res => res
      .pipe(unzip.Parse())
      .on('entry', entry => {
        if (entry.type === 'File' && entry.path === filename)
          entry.pipe(createWriteStream(path.join(WEBDRIVER_DIR, filename), { mode: 0o755 }));
        else
          entry.autodrain();
      })
      .on('finish', onFinish)
  }
  throw new Error('Unsupported URL');
}
