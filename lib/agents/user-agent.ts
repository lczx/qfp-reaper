import { Builder } from 'selenium-webdriver';

import { getBrowserProvider, getWebDriverProvider } from '../providers';
import { AgentConfig, BrowserType, config } from '../config';


abstract class UserAgent {

  protected driverPath?: string;
  protected browserPath?: string;

  constructor(public readonly agentConfig: AgentConfig,
              public readonly browserType: BrowserType,
              public readonly browserVersion: string) { }

  protected abstract makeBuilder(profileDir: string): Builder;

  protected abstract onQuit(): Promise<void>;

  public async configure(): Promise<void> {
    this.driverPath = await getWebDriverProvider(this.browserType)();
    this.browserPath = await getBrowserProvider(this.browserType)(this.browserVersion);
  }

  public async navigate(profileDir: string, url: string) {
    if (this.driverPath == null || this.browserPath == null)
      throw new Error('User agent has not been initialized');

    const driver = await this.makeBuilder(profileDir).build();

    try {
      await driver.get(url);
      await driver.wait(
        () => driver.executeScript('return document.readyState').then(v => v === 'complete'),
        config.pageLoadTimeout);

    } finally {
      await driver.quit();
      await this.onQuit();
    }
  }

  public toString() {
    return `${this.browserType}@${this.browserVersion}`;
  }

}

export default UserAgent;
