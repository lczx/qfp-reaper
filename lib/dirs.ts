import path from 'path';

import { config } from './config';


export const WEBDRIVER_DIR = path.join(config.workDir, 'webdriver');
export const BROWSERS_DIR = path.join(config.workDir, 'browsers');
export const SESSIONS_DIR = path.join(config.workDir, 'sessions');
