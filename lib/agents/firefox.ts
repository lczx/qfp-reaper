import { Builder } from 'selenium-webdriver';
import { Options, ServiceBuilder } from 'selenium-webdriver/firefox';

import UserAgent from './user-agent';
import { FirefoxConfig } from '../config';


class FirefoxAgent extends UserAgent {

  constructor(agentConfig: FirefoxConfig) {
    super(agentConfig, 'firefox', (agentConfig as FirefoxConfig).version);
  }

  protected makeBuilder(profileDir: string): Builder {
    const config = this.agentConfig as FirefoxConfig;

    const options = new Options();
    if (config.preferences != null) {
      for (const [key, value] of Object.entries(config.preferences))
        options.setPreference(key, value);
    }

    return new Builder()
      .forBrowser('firefox')
      .setFirefoxService(new ServiceBuilder(this.driverPath!))
      .setFirefoxOptions(options
        .setBinary(this.browserPath!)
        .addArguments('-profile', profileDir)
        .headless());
  }

  protected async onQuit() {
    // No-op
  }

}

export default FirefoxAgent;
