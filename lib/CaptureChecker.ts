import { Logger } from 'winston';

import { CaptureAlert } from './config';
import { waitProcessOutput } from './process';
import { findInPath } from './util';

export const TSHARK_BINARY = 'tshark' + (process.platform === 'win32' ? '.exe' : '');

class CaptureChecker {

  private readonly binaryPath;
  private readonly alerts;

  constructor(alerts: CaptureAlert[] | null) {
    this.alerts = alerts || [];

    const binaryPath = findInPath(TSHARK_BINARY);
    if (binaryPath == null)
      throw new Error(`Capture analysis binary "${TSHARK_BINARY}" not found`);
    this.binaryPath = binaryPath;

    for (const alert of this.alerts)
      if (!['absent', 'present'].includes(alert.condition))
        throw new Error(`Unknown alert condition "${alert.condition}"`);
  }

  async checkCapture(logger: Logger, tag: string, localIp: string, hostIp: string, pcapFile: string, keylogFile?: string): Promise<CaptureAlert[]> {
    const matchingAlerts = [];

    for (const alert of this.alerts) {
      const ret = await this.checkAlert(alert, localIp, hostIp, pcapFile, keylogFile);
      const retLines = ret.split('\n').length - 1;

      if ((alert.condition === 'absent' && retLines === 0) ||
          (alert.condition === 'present' && retLines > 0)) {
        logger.warn(`[${tag}] ${alert.message}`);
        matchingAlerts.push(alert);
      }
    }

    return matchingAlerts;
  }

  private checkAlert(alert: CaptureAlert, localIp: string, hostIp: string, pcapFile: string, keylogFile?: string): Promise<string> {
      const filter = alert.filter
        .replaceAll('{LOCAL_IP}', localIp)
        .replaceAll('{REMOTE_IP}', hostIp);

      const processArgs = [];
      if (keylogFile != null)
        processArgs.push('-o', `tls.keylog_file:${keylogFile}`);
      processArgs.push( '-Y', filter, '-r', pcapFile);

      return waitProcessOutput(this.binaryPath, processArgs);
  }

}

export default CaptureChecker;
