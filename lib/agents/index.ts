import { AgentConfig } from '../config';
import UserAgent from './user-agent';
import ChromiumAgent from './chromium';
import FirefoxAgent from './firefox';


export const createAgent = (agentConfig: AgentConfig): UserAgent => {
  if (agentConfig.type === 'chromium')
    return new ChromiumAgent(agentConfig);
  if (agentConfig.type === 'firefox')
    return new FirefoxAgent(agentConfig);
  throw new Error(`Unknown agent type: ${agentConfig['type']}`);
};

export { default as UserAgent } from './user-agent';
