import path from 'path';
import fs from 'fs';


export const fileExists = (path: string) => fs.existsSync(path) && fs.statSync(path).isFile();

export const dirExists = (path: string) => fs.existsSync(path) && fs.statSync(path).isDirectory();

export function findInPath(fileName: string): string | null {
  const pathDirs = process.env['PATH']!.split(path.delimiter);

  const dirs = process.platform === 'win32'
    ? [ 'C:\\Program FIles\\Wireshark', ...pathDirs ]
    : pathDirs;

  const targetDir = dirs.find(d => {
    const fullPath = path.join(d, fileName);
    try {
      return fs.statSync(fullPath).isFile();
    } catch (e) {
      return false;
    }
  });

  return targetDir != null ? path.join(targetDir, fileName) : null;
}
