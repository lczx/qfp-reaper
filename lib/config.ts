import { readFileSync } from 'fs';

import { parse as parseYaml} from 'yaml';


type ReaperConfig = {
  workDir: string,
  urls: string,
  pageLoadTimeout: number,
  captureEndDelay?: number,
  attempts: number,
  timeBetweenCaptures?: number,
  timeBetweenAttempts?: number,
  ignoreHosts: string[],
  agents: AgentConfig[],
  alerts: CaptureAlert[] | null
}

export type BrowserType = 'firefox' | 'chromium';

export type AgentConfig = { type: BrowserType, profileKey?: string } & (FirefoxConfig | ChromiumConfig);

export type FirefoxConfig = { type: 'firefox', version: string, preferences?: { [key: string]: any } };
export type ChromiumConfig = { type: 'chromium', version: string };

export type CaptureAlert = {
  message: string,
  filter: string,
  condition: 'absent' | 'present',
  retry?: boolean,
  critical?: boolean
};

export type UrlMeta = { url: string, metadata: object | null };

export const config: ReaperConfig = (() => {
  const data = readFileSync('./config.yml', { encoding: 'utf8' });
  return parseYaml(data);
})();
