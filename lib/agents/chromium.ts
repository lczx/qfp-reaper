import fs from 'fs/promises';
import os from 'os';
import path from 'path';
import { Builder } from 'selenium-webdriver';
import { Options, ServiceBuilder } from 'selenium-webdriver/chrome';

import UserAgent from './user-agent';
import { ChromiumConfig } from '../config';


const DISABLE_SHM = true;

class ChromiumAgent extends UserAgent {

  constructor(agentConfig: ChromiumConfig) {
    super(agentConfig, 'chromium', (agentConfig as ChromiumConfig).version);
  }

  protected makeBuilder(profileDir: string): Builder {
    return new Builder()
      .forBrowser('chrome')
      .setChromeService(new ServiceBuilder(this.driverPath!))
      .setChromeOptions(new Options()
        .setChromeBinaryPath(this.browserPath!)
        .addArguments((DISABLE_SHM ? '--disable-dev-shm-usage ' : '') + `--user-data-dir=${profileDir}`)
        .headless());
  }

  protected async onQuit() {
    if (!DISABLE_SHM) return;

    for (let d of await fs.readdir(os.tmpdir(), { withFileTypes: true })) {
      if (d.isDirectory() && d.name.startsWith('.com.google.Chrome.'))
        await fs.rm(path.join(os.tmpdir(), d.name), { recursive: true });
    }
  }

}

export default ChromiumAgent;
