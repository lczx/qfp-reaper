import os from 'os';
import path from 'path';
import { readdirSync } from 'fs';
import { cp, mkdir, rename, rm } from 'fs/promises';

import { UserAgent } from './agents';
import {dirExists} from './util';


const PROFILES_DIR = path.join(os.tmpdir(), 'rp-profiles');
const PENDING_PROFILE_DIR = path.join(os.tmpdir(), 'rp-profile-pending');

const randString = (length: number) =>
  [...Array(length)].map(_ => (~~(Math.random() * 36)).toString(36)).join('');

class ProfileManager {

  private readonly profiles: string[];
  private pendingProfile?: string;

  constructor() {
    // Restore from existing profile dirs
    try {
      this.profiles = readdirSync(PROFILES_DIR, { withFileTypes: true })
        .filter(e => e.isDirectory())
        .map(e => path.join(PROFILES_DIR, e.name));
    } catch (_) {
      this.profiles = [];
    }
  }

  async getProfileDir({ browserType, browserVersion, agentConfig }: UserAgent): Promise<[dir: string, fresh: boolean]> {
    if (dirExists(PENDING_PROFILE_DIR))
      await rm(PENDING_PROFILE_DIR, { recursive: true });

    const profileKey = agentConfig.profileKey != null ? agentConfig.profileKey : randString(6);
    const dirName = path.join(PROFILES_DIR, `${browserType}-${browserVersion}-${profileKey}`);
    this.pendingProfile = dirName;

    if (this.profiles.includes(dirName)) {
      await cp(dirName, PENDING_PROFILE_DIR, { recursive: true })
      return [PENDING_PROFILE_DIR, false];
    }

    await mkdir(PENDING_PROFILE_DIR, { recursive: true });
    return [PENDING_PROFILE_DIR, true];
  }

  async commit() {
    if (this.pendingProfile == null)
      throw new Error('No pending profile');

    if (!dirExists(PROFILES_DIR))
        await mkdir(PROFILES_DIR);

    if (dirExists(this.pendingProfile))
      await rm(this.pendingProfile, { recursive: true });
    else
      this.profiles.push(this.pendingProfile);

    await rename(PENDING_PROFILE_DIR, this.pendingProfile);
    this.pendingProfile = undefined;
  }

  async cleanup() {
    while (this.profiles.length > 0)
      await rm(this.profiles.pop()!, { recursive: true });
  }

}

export async function wipeProfileData() {
  await rm(PROFILES_DIR, { force: true, recursive: true });
  await rm(PENDING_PROFILE_DIR, { force: true, recursive: true });
}

export default ProfileManager;
